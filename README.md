# container

Contains different non-usual containers implementations like a bitfield, cache, etc.  

# Documentation
see [pipelines](https://gitlab.com/chrklitz_public/container/-/pipelines) to download the documentation.

## lru_cache concept:
The lru-cache implementation may be over-the-top complicated but this class is more about the principle 
of implementing an lru-cache which is completely continuous in space and not scattered around like 
the nodes in a linked list. You could probably recycle nodes to prevent this, but this implementation 
essentially does the same but inside a single array which guarantees a fixed number of cache misses 
because the array should get loaded completely at once most of the time.

## bounded_int_sequence:
This is a special case of a doubly-linked-list where the pointer to the next and the previous elements are stored inside a single
array with the array indexes representating the numbers and the entries the pointer.


## bitfield concept:
This is an implementation for a bitfield (class bitfield), therefore a memory-efficient representation for a set of integer values. 
The special thing about this class is, that it supports the std bidirectional const_iterator. Note that
the memory-efficiency depends on the selected encoding-strategy and also the inserted numbers.

The container is not generic by allowing any value as a template parameter but its template parameter for the value-type allows selection of 
different integer types like uint32_t, unsigned long, int64_t, etc. The container then 
automatically selects this type as the internal representation for the bits. Furthermore the stored numbers are of the selected type.
Note that the number_set class also allows signed integers. This is reached
by allowing the bitfied implementation to select an encoding strategy at compile-time, 
which bidirectionaly maps bitfield-indexes onto numbers and reverse. 
Since this class supports a bidirectional const_iterator this should make it 
compatible to a lot of std::iterator based algorithms and also to the c++ for-loop.
Also note that the bitfield-implementation is technically a set and not a list of integers and
the iteration-order depends on the
selected encoding-strategy and normally not on the insertion-order. Only some strategies allow sorted iteration.



# LICENSE
Look at the LICENSE file if existing. With regard to the LICENSE feel free to use this library and change files if allowed but it would be 
nice if you leave the comments at the top of the files.
