#include "container/lru_cache.hpp"
#include <iostream>

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(cache_tests)
{
  lru_cache<size_t, uint8_t> id_cache(3);
  cache_result<size_t> res;


  // some default tests
  BOOST_REQUIRE(id_cache.max_size() == 3);
  BOOST_CHECK(!id_cache.is_cached(0));
  BOOST_CHECK(!id_cache.is_cached(std::numeric_limits<size_t>::max()));


  // check correct uncaching and caching results:
  res = id_cache.cache(10);
  BOOST_CHECK(!res.uncached && !res.was_cached);
  BOOST_CHECK(id_cache.is_cached(10));

  res = id_cache.cache(42);
  BOOST_CHECK(!res.uncached && !res.was_cached);
  BOOST_CHECK(id_cache.is_cached(42));

  res = id_cache.cache(10);
  BOOST_CHECK(res.was_cached && !res.uncached);
  BOOST_CHECK(id_cache.is_cached(10));

  BOOST_CHECK_MESSAGE(id_cache.size() == 2, "Re-caching 10 should not increase size to 3");

  res = id_cache.cache(128);
  BOOST_CHECK(!res.uncached && !res.was_cached);
  BOOST_CHECK(id_cache.is_cached(128));
  BOOST_CHECK_MESSAGE(id_cache.is_cached(10),
                      "size of cache should still be 3 and therefore "
                      "the cache should be big enough -> no uncache required");
  BOOST_CHECK_MESSAGE(id_cache.is_cached(42),
                      "size of cache should still be 3 and therefore "
                      "the cache should be big enough -> no uncache required");

  // assert cache looks like [128, 10, 42] with cache-recency dropping from left to right
  res = id_cache.cache(11);
  BOOST_CHECK(!res.was_cached);
  BOOST_CHECK_MESSAGE(res.uncached,
                      "A cache call with a previously "
                      "uncached insert value to a full cache should result "
                      "in another value being uncached");
  BOOST_CHECK_MESSAGE(res.uncached_value == 42,
                      "The uncached value should be 42 "
                      "because it is the least recently used value in the cache");
  BOOST_CHECK(!id_cache.is_cached(42));

  // assert cache looks like [11, 128, 10]
  res = id_cache.cache(10);
  BOOST_CHECK(res.was_cached);
  BOOST_CHECK(!res.uncached);

  // assert cache looks like [10, 11, 128]
  res = id_cache.cache(42);
  BOOST_CHECK(!res.was_cached);
  BOOST_CHECK(res.uncached && res.uncached_value == 128);

  // assert cache looks like [42, 10, 11]
  BOOST_CHECK(id_cache.is_cached(42));
  BOOST_CHECK(id_cache.is_cached(10));
  BOOST_CHECK(id_cache.is_cached(11));
  BOOST_CHECK(!id_cache.is_cached(128));


  // check cache flush:
  id_cache.flush();
  BOOST_CHECK_MESSAGE(id_cache.size() == 0, "cache should be empty");
  BOOST_CHECK(!id_cache.is_cached(128));
  BOOST_CHECK(!id_cache.is_cached(10));
  BOOST_CHECK(!id_cache.is_cached(42));
  BOOST_CHECK(!id_cache.is_cached(0));
}
