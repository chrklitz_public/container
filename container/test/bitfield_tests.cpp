#include "container/dynamic_bitfield.hpp"

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(unsigned_strat)
{
  dynamic_bitfield<uint8_t> set;
  BOOST_CHECK_MESSAGE(!set.contains(0), "set should be empty");
  BOOST_CHECK_MESSAGE(set.begin() == set.end(), "set should be empty");

  set.insert(10);
  BOOST_CHECK_MESSAGE(set.contains(10), "set should contain the number 10");

  set.erase(10);
  BOOST_CHECK_MESSAGE(!set.contains(10), "set should be empty");

  set.erase(42);
  BOOST_CHECK_MESSAGE(set.begin() == set.end(), "set should still be empty");

  set.insert(0);
  set.insert(11);
  set.insert(255);
  BOOST_CHECK_MESSAGE(set.contains(0), "set should contain number 0");
  BOOST_CHECK_MESSAGE(set.contains(11), "set should contain number 11");
  BOOST_CHECK_MESSAGE(set.contains(255), "set should contain number 255 which is the highest possible"
                                         "value in an number_set<uint8_t>");
  BOOST_CHECK_MESSAGE(!set.contains(42), "set should not contain value 42");

  auto i1 = set.begin();
  auto i2 = set.begin();

  BOOST_CHECK_MESSAGE(i1 == i2, "set.begin() return value should be equal for every call");
  BOOST_CHECK_MESSAGE((i1++) == i2, "post-increment should return not incremented iterator");

  i1 = set.end();
  i2 = set.end();
  i1--;
  --i2;

  BOOST_CHECK_MESSAGE(i1 == i2, "pre- and post-decrement should result in the same");
  BOOST_CHECK_MESSAGE(--(++i1) == i2, "pre- increment and decrement should be inverse");
  BOOST_CHECK_MESSAGE(++(--i1) == i2, "pre- decrement and increment should be inverse");

  BOOST_CHECK_MESSAGE(i1 == i2,
                      "something is wrong with the pre-increment and decrement operators of class number_set");
}

BOOST_AUTO_TEST_CASE(signed_strat)
{
  dynamic_bitfield<int8_t> set;
  BOOST_CHECK_MESSAGE(!set.contains(0), "set should be empty");
  BOOST_CHECK_MESSAGE(set.begin() == set.end(), "set should be empty");

  set.insert(10);
  BOOST_CHECK_MESSAGE(set.contains(10), "set should contain the number 10");

  set.erase(10);
  BOOST_CHECK_MESSAGE(!set.contains(10), "set should be empty");

  set.erase(42);
  BOOST_CHECK_MESSAGE(set.begin() == set.end(), "set should still be empty");

  set.insert(0);
  set.insert(11);
  set.insert(127);
  set.insert(-128);

  BOOST_CHECK_MESSAGE(set.contains(0), "set should contain number 0");
  BOOST_CHECK_MESSAGE(set.contains(11), "set should contain number 11");
  BOOST_CHECK_MESSAGE(set.contains(127),
                      "set should contain number 127 which is the highest possible"
                      "value in an number_set<int8_t>");
  BOOST_CHECK_MESSAGE(set.contains(-128),
                      "set should contain number -128 which is the lowest possible"
                      "value in an number_set<int8_t>");
  BOOST_CHECK_MESSAGE(!set.contains(42), "set should not contain value 42");

  auto i1 = set.begin();
  auto i2 = set.begin();

  BOOST_CHECK_MESSAGE(i1 == i2, "set.begin() return value should be equal for every call");
  BOOST_CHECK_MESSAGE((i1++) == i2, "post-increment should return not incremented iterator");

  i1 = set.end();
  i2 = set.end();
  i1--;
  --i2;

  BOOST_CHECK_MESSAGE(i1 == i2, "pre- and post-decrement should result in the same");
  BOOST_CHECK_MESSAGE(--(++i1) == i2, "pre- increment and decrement should be inverse");
  BOOST_CHECK_MESSAGE(++(--i1) == i2, "pre- decrement and increment should be inverse");

  BOOST_CHECK_MESSAGE(
    i1 == i2,
    "something is wrong with the pre-increment and decrement operators of class number_set");
}

BOOST_AUTO_TEST_CASE(signed_ascending_strat)
{
  dynamic_bitfield<int8_t, numset_signed_ascending_strategy> set;
  set.insert(-128);
  set.insert(127);

  BOOST_CHECK_MESSAGE(set.contains(-128), "set should contain -128");
  BOOST_CHECK_MESSAGE(set.contains(127), "set should contain 127");
  BOOST_CHECK_MESSAGE(!set.contains(42), "set should not contain value 42");
}
