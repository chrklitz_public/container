#include "container/bounded_int_sequence.hpp"


#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(push_back_tests)
{
  bounded_int_sequence<uint8_t> seq;
  static constexpr uint8_t num_max = std::numeric_limits<uint8_t>::max();
  std::vector<uint8_t> ref;
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref, "seqence should be empty");

  seq.push_back(10);
  ref.push_back(10);
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref, "seq should only contain one number with value 10");

  seq.push_back(0);
  ref.push_back(0);

  seq.push_back(num_max);
  ref.push_back(num_max);
  BOOST_CHECK(seq.to_vector() == ref);

  seq.push_back(10);
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref,
                      "Re-adding a value which already was in the sequence "
                      "should not make any difference because each value needs "
                      "to be unique");

  seq.clear();
  ref.clear();
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref,
                      "clearing should remove all elements and reset the data-structure "
                      "entirely");

  seq.push_back(num_max);
  ref.push_back(num_max);
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref,
                      "Although the numeric maximum of the used integral-type "
                      "is a special case, when the sequence is empty, it should be "
                      "handled correctly");

  seq.push_back(0);
  ref.push_back(0);
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref, "Adding zero after the numeric maximum should be okay "
                                              "because addition of the numeric maximum before should not "
                                              "influence the state to produce undefined behaviour");
}

BOOST_AUTO_TEST_CASE(contains_tests)
{
  bounded_int_sequence<uint8_t> seq;

  BOOST_CHECK(!seq.contains(10));
  BOOST_CHECK(!seq.contains(0));
  BOOST_CHECK(!seq.contains(255));

  seq.push_back(42);
  BOOST_CHECK_MESSAGE(seq.contains(42), "sequence should contain the value");
  BOOST_CHECK(!seq.contains(0));

  static constexpr uint8_t num_max = std::numeric_limits<uint8_t>::max();
  seq.clear();
  BOOST_CHECK(!seq.contains(num_max));

  seq.push_back(num_max);
  BOOST_CHECK_MESSAGE(seq.contains(num_max),
                      "Adding the numeric maximum should be fine although it is "
                      "a special case");

  BOOST_CHECK(!seq.contains(10));

  seq.push_back(10);
  BOOST_CHECK_MESSAGE(seq.contains(10),
                      "sequence should now contain the value although it "
                      "was added after the numeric maximum which is a special case");

  BOOST_CHECK(!seq.contains(12));
  seq.push_back(12);
  BOOST_CHECK(seq.contains(12));

  BOOST_CHECK(!seq.contains(122));
  seq.push_back(122);
  BOOST_CHECK(seq.contains(122));
}

BOOST_AUTO_TEST_CASE(erase_tests)
{
  bounded_int_sequence<uint8_t> seq;
  static constexpr uint8_t num_max = std::numeric_limits<uint8_t>::max();
  std::vector<uint8_t> ref;


  // when empty nothing should be erase-able
  BOOST_CHECK(!seq.erase(0));
  BOOST_CHECK(!seq.erase(42));
  BOOST_CHECK(!seq.erase(num_max));


  // The clear before each block is just for safety if
  // erase does not work as wanted this won't influence later tests

  const char *add_remove_empty_msg
    = "adding and then removing a value from an empty sequence should "
      "result in an empty sequence";

  seq.clear();
  seq.push_back(42);
  BOOST_CHECK(seq.erase(42));
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref, add_remove_empty_msg);

  seq.clear();
  seq.push_back(0);
  BOOST_CHECK(seq.erase(0));
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref, add_remove_empty_msg);

  seq.clear();
  seq.push_back(num_max);
  BOOST_CHECK(seq.erase(num_max));
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref, add_remove_empty_msg);


  // tests with more than one value at a time:
  seq.clear();

  seq.push_back(0);
  ref.push_back(0);

  seq.push_back(20);
  ref.push_back(20);

  seq.push_back(num_max);
  ref.push_back(num_max);

  // numbers which are not in the sequence should not be erase-able
  BOOST_CHECK(!seq.erase(1));
  BOOST_CHECK(!seq.erase(42));
  BOOST_CHECK(!seq.erase(243));

  // the added values abouth should be erase-able
  BOOST_CHECK(seq.erase(0));
  ref.erase(std::find(ref.begin(), ref.end(), 0));
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref, "removing one of the values aboth at a time should be fine");

  BOOST_CHECK(seq.erase(20));
  ref.erase(std::find(ref.begin(), ref.end(), 20));
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref, "removing one of the values aboth at a time should be fine");

  BOOST_CHECK(seq.erase(num_max));
  ref.erase(std::find(ref.begin(), ref.end(), num_max));
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref, "removing one of the values aboth at a time should be fine");

  // numbers which are not in the sequence should not be erase-able
  BOOST_CHECK(!seq.erase(0));
  BOOST_CHECK(!seq.erase(20));
  BOOST_CHECK(!seq.erase(num_max));

  // Check edge case of num_max and emptiness of the sequence
  seq.clear();
  seq.push_back(num_max);
  seq.push_back(0);
  seq.erase(0);
  ref = {num_max};
  BOOST_CHECK(seq.to_vector() == ref);
  seq.erase(num_max);
  ref.clear();
  BOOST_CHECK(seq.to_vector() == ref);
}

BOOST_AUTO_TEST_CASE(insert_tests)
{
  bounded_int_sequence<uint8_t> seq;
  static constexpr uint8_t num_max = std::numeric_limits<uint8_t>::max();
  std::vector<uint8_t> ref;

  BOOST_REQUIRE(seq.push_back(0));
  BOOST_REQUIRE(seq.insert(0, 42));
  ref = {42, 0};
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref,
                      "inserting value before first value should make it the "
                      "the new start of the sequence");

  seq.clear();
  seq.push_back(42);
  seq.push_back(0);
  ref = {42, 0};
  BOOST_REQUIRE(seq.to_vector() == ref);

  BOOST_REQUIRE(seq.insert(0, num_max));
  ref = {42, num_max, 0};

  BOOST_CHECK_MESSAGE(seq.to_vector() == ref, "inserting value in the middle of the sequence "
                                              "should be fine");
}

BOOST_AUTO_TEST_CASE(make_begin_tests)
{
  bounded_int_sequence<uint8_t> seq;
  static constexpr uint8_t num_max = std::numeric_limits<uint8_t>::max();
  std::vector<uint8_t> ref;

  seq.push_back(42);
  seq.push_back(0);
  BOOST_CHECK(seq.make_begin(0));
  ref = {0, 42};
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref,
                      "Making 0 the new start of [42, 0] should result in the sequence "
                      "[0, 42]");

  seq.clear();
  ref.clear();
  seq.push_back(42);
  seq.push_back(num_max);
  seq.push_back(0);
  BOOST_CHECK(seq.make_begin(num_max));
  ref = {num_max, 0, 42};
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref,
                      "Making num_max to the start of sequence [42, num_max, 0] "
                      "should result in the sequence [num_max, 0, 42]");

  // simulate insert of num_max before 0:
  seq.clear();
  ref.clear();
  seq.push_back(10);
  seq.push_back(0);
  BOOST_CHECK(seq.make_begin(0));
  BOOST_CHECK(seq.push_back(num_max));
  ref = {0, 10, num_max};
  BOOST_CHECK(seq.to_vector() == ref);
  BOOST_CHECK(seq.make_begin(10));
  ref = {10, num_max, 0};
  BOOST_CHECK_MESSAGE(seq.to_vector() == ref, "The abouth procuedure should essentially insert "
                                              "num_max before 0");
}
