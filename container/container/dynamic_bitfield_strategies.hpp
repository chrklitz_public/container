/*
 * Author: Christopher von Klitzing
 * Repository: https://gitlab.com/chrklitz_public/bitfield-std-like-container
 * License: See the LICENSE file in the git-repository
 */
#ifndef DYNAMIC_BITFIELD_STRATEGIES_H
#define DYNAMIC_BITFIELD_STRATEGIES_H

#include <type_traits>
#include <limits>
#include "dynamic_bitfield_iterator.hpp"

template <typename number_t>
struct numset_zerocentered_strategy {
  static_assert (std::is_signed<number_t>::value,
                 "Selected an unsigned-integral type in a strategy which only supports signed-integral types.");

private:
  using unumber_t = typename std::make_unsigned<number_t>::type;
  // using self_type = numset_zerocentered_strategy<number_t>;

public:
  inline static unumber_t number_to_index(const number_t &num) { // overflow proof
    if (num < 0) {
      unumber_t pnum = static_cast<unumber_t>(-(num + 1)) + 1; // pnum=abs(num)
      return (pnum - 1) + pnum; // -2*num - 1
    } else {
      return 2 * num;
    }
  }

  inline static number_t index_to_number(const unumber_t &unum) { // inverse to function number_to_index(...)
    if (unum % 2 == 0) { // return number must be positive
      return static_cast<number_t>(unum / 2);
    } else { // return number must be negative
      return -(static_cast<number_t>((unum - 1) / 2) + 1);
    }
  }

  template<typename iterator>
  class const_iterator_sorted {

  };
};

template <typename number_t>
struct numset_unsigned_ascending_strategy {
  static_assert (std::is_unsigned<number_t>::value,
                 "Selected a signed-integral type in a strategy which only supports unsigned-integral types.");

public:
  inline static number_t  number_to_index(const number_t &num) {
    return num;
  }
  inline static number_t index_to_number(const number_t &num) {
    return num;
  }

  using is_sorted = std::true_type;
};

template <typename number_t>
struct numset_signed_ascending_strategy {
  static_assert (std::is_signed<number_t>::value,
                 "Selected an unsigned-integral type in a strategy which only supports signed-integral types.");
private:
  using unumber_t = typename std::make_unsigned<number_t>::type;
public:
  inline static unumber_t number_to_index(const number_t &num) {
    if (num < 0) {
      return static_cast<unumber_t>((num + -1* (std::numeric_limits<number_t>::min() + 1)) + 1);
    } else {
      return static_cast<unumber_t>(num) + static_cast<unumber_t>(-(std::numeric_limits<number_t>::min() + 1)) + 1;
    }
  }
  inline static number_t index_to_number(const unumber_t &num) {
    if (num <= static_cast<unumber_t>(std::numeric_limits<number_t>::max())) {
      return (static_cast<number_t>(num) - std::numeric_limits<number_t>::max()) - 1;
    } else {
      return static_cast<number_t>(num - static_cast<unumber_t>(std::numeric_limits<number_t>::max())) - 1;
    }
  }

  using is_sorted = std::true_type;
};

// ==========================================

template <typename number_t>
using default_strategy = typename std::conditional<std::is_unsigned<number_t>::value,
                                       numset_unsigned_ascending_strategy<number_t>,
                                       numset_zerocentered_strategy<number_t>>::type;

#endif // DYNAMIC_BITFIELD_STRATEGIES_H
