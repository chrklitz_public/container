/*
 * Author: Christopher von Klitzing
 * Repository: https://gitlab.com/chrklitz_public/bitfield-std-like-container
 * License: See the LICENSE file in the git-repository
 */
#ifndef DYNAMIC_BITFIELD_ITERATOR_H
#define DYNAMIC_BITFIELD_ITERATOR_H

#include <type_traits>
#include <limits>
#include <stdexcept>

template <typename Set> // number_set with selected template parameters
class const_iterator_native
{
public:
  using self_type = const_iterator_native;
  using value_type = typename Set::value_type;
  using reference = value_type;
  using pointer = value_type *;
  using difference_type = typename std::make_unsigned<value_type>::type;
  using iterator_category = std::bidirectional_iterator_tag;

private:
  using index_t = typename std::make_unsigned<value_type>::type;

private:
  inline unsigned int find_rightmost_bit(index_t n) { // finds the right-most set bit
    if (n == 0)
      return Set::atomsize;

    unsigned int pos = 0;
    index_t filter = 1;

    while ((n & filter) == 0) {
      filter = static_cast<index_t>(filter << static_cast<index_t>(1));
      ++pos;
    }
    return pos;
  }

  inline unsigned int find_leftmost_bit(index_t n) {
    if (n == 0)
      return Set::atomsize;

    unsigned int pos = Set::atomsize - 1;
    index_t filter = static_cast<index_t>(1) << (Set::atomsize - 1);

    while ((n & filter) == 0) {
      filter = filter >> 1;
      --pos;
    }
    return pos;
  }

  // If return value equals startpoint it is not guaranteed to point to a set bit.
  // If the return value is unequal startpoint it must point to a set bit.
  index_t find_next(const index_t &startpoint) { // startpoint inclusive in search
    for (index_t q = startpoint / Set::atomsize; q < set->atom_count(); ++q) {
      if (set->data[q] != 0) {
        int clear = (q == startpoint / Set::atomsize) ? startpoint % Set::atomsize : 0;

         // the inner stuff sets clear-number of bits from the right to zero
        int offset = find_rightmost_bit((static_cast<index_t>(-1) << clear) & static_cast<index_t>(set->data[q]));
        if (offset != Set::atomsize) {
          return q * Set::atomsize + offset;
        }
      }
    }
    return startpoint;
  }

  index_t find_prev(const index_t &startpoint) { // startpoint inclusive in search
    index_t q;
    index_t sp_field = std::min(startpoint / Set::atomsize, set->atom_count() - 1);
    for (index_t qr = 0; qr <= sp_field; qr++) {
      q = sp_field - qr;
      int clear = (q == sp_field) ? startpoint % Set::atomsize + 1 : Set::atomsize;

      // the inner stuff sets clear-number of bits from the left to zero
      int offset = find_leftmost_bit(~(static_cast<index_t>(-1) << clear) & static_cast<index_t>(set->data[q]));
      if (offset != Set::atomsize) {
        return q * Set::atomsize + offset;
      }
    }
    return startpoint;
  }

  inline void move_next() {
    // check for current state. check if state is --set->end() and *(--set->end()) == -2^(Set::atomsize-1)
    if (!is_end && current == static_cast<index_t>(-1) ) {
      is_end = true;
      return;
    }

    index_t next = find_next(current + 1);
    if (current + 1 == next && !set->contains_index(next)) {
      current = static_cast<index_t>(-1);
      is_end = true;
    } else {
      current = next;
    }
  }

  inline void move_prev() {
    if (is_end && current == static_cast<index_t>(-1)) { // current state: set->end()
      index_t last = find_prev(current);
      if (!(last == current && !set->contains_index(last))) {
        is_end = false;
        current = last;
        return;
      }
      // list must be empty:
      return;
    }

    index_t prev = find_prev(current - 1);
    if (current - 1 == prev && !set->contains_index(prev)) { // not found any before
      // undefined behaviour because there should not exist an iterator before set->begin()
      // current implementation is, that it remains on begin() until incrementation
    } else {
      current = prev;
    }
  }

public:
  // constructs iterator which starts at the next set bit beginning at the specified start index
  const_iterator_native(const Set *numset, index_t start) : set(numset), current(find_next(start)) {
    if (start == current && !set->contains_index(current)) {
      current = static_cast<index_t>(-1);
      is_end = true;
    }
  }

  const_iterator_native(const Set *numset) : set(numset), current(static_cast<index_t>(-1)), is_end(true) {} // end constructor

  self_type &operator++() {
    move_next();
    return *this;
  }
  self_type operator++(int junk) {
    (void) junk;
    self_type old = *this;
    move_next();
    return old;
  }

  self_type &operator--() {
    move_prev();
    return *this;
  }
  self_type operator--(int junk) {
    (void) junk;
    self_type old = *this;
    move_prev();
    return old;
  }

  reference operator*() const {
    if (is_end)
      throw std::out_of_range("iterator is out of range");
    return Set::strategy::index_to_number(current);
  }

  pointer operator->() const {
    return nullptr;
  }

  bool operator==(const self_type &other) const {
    return (set == other.set && current == other.current && is_end == other.is_end);
  }
  bool operator!=(const self_type &other) const {
    return (set != other.set || current != other.current || is_end != other.is_end);
  }

private:
  const Set *set;
  index_t current;
  bool is_end = false;
};


#endif // DYNAMIC_BITFIELD_ITERATOR_H
