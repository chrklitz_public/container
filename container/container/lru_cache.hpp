/*
 * Author: Christopher von Klitzing
 * Repository: https://gitlab.com/chrklitz_public/fixed-size-containers
 * License: See the LICENSE file in the git-repository
 */
#ifndef LRU_CACHE_H
#define LRU_CACHE_H

#include "bounded_int_sequence.hpp"

template<typename T>
struct cache_result
{
  T uncached_value = {0};
  bool uncached = false;
  bool was_cached = false;
};

/*!
 * Least-recently-used cache implementation with (average) time-complexity O(1) for all operations.
 * Furthermore all used data-structures except the std::unordered_map are completely continuous in memory
 * and have O(max_size()) space-complexity which is not more than regular cache implementations with a
 * linked list and a hash map. One possible drawback could be the missing uncache function for manual use
 * but that is probably not always needed.
 */
template<typename T, typename id_t = size_t>
class lru_cache
{
public:
  lru_cache(id_t size);

  cache_result<T> cache(const T &entry);
  bool is_cached(const T &value) const;
  void flush();

  inline id_t max_size() const { return maximum_size; }
  inline id_t size() const { return value_to_id_map.size(); }

private:
  bounded_int_sequence<id_t> bisequence; // doubly-linked-list without any entries
  std::vector<T> id_to_value_map;
  std::unordered_map<T, id_t> value_to_id_map;
  const id_t maximum_size;
};

// ================ CLASS IMPLEMENTATION ==================

template<typename T, typename id_t>
lru_cache<T, id_t>::lru_cache(id_t size) : maximum_size(size)
{
  id_to_value_map.resize(max_size());
  value_to_id_map.reserve(max_size());
  bisequence.reserve(max_size());
}

template<typename T, typename id_t>
cache_result<T> lru_cache<T, id_t>::cache(const T& entry)
{
  cache_result<T> res;

  if (!is_cached(entry)) {
    id_t insert_id = size();

    if (size() >= max_size()) {
      insert_id = bisequence.back();
      T &old = id_to_value_map[insert_id];

      res.uncached = true;
      res.uncached_value = old;

      bisequence.erase(insert_id);
      value_to_id_map.erase(old);
    }

    bisequence.push_front(insert_id);
    id_to_value_map[insert_id] = entry;
    value_to_id_map[entry] = insert_id;
  } else {
    res.was_cached = true;
    bisequence.bring_to_front(value_to_id_map[entry]);
  }
  return res;
}

template<typename T, typename id_t>
bool lru_cache<T, id_t>::is_cached(const T& value) const
{
  return value_to_id_map.find(value) != value_to_id_map.end();
}

template<typename T, typename id_t>
void lru_cache<T, id_t>::flush()
{
  bisequence.clear(false);
  id_to_value_map.clear();
  id_to_value_map.resize(max_size());
  value_to_id_map.clear();
  value_to_id_map.reserve(max_size());
}


#endif // LRU_CACHE_H
