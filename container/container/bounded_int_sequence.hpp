/*
 * Author: Christopher von Klitzing
 * Repository: https://gitlab.com/chrklitz_public/fixed-size-containers
 * License: See the LICENSE file in the git-repository
 */
#ifndef BOUNDED_INT_SEQUENCE_H
#define BOUNDED_INT_SEQUENCE_H

#include <vector>
#include <unordered_map>
#include <functional>
#include <type_traits>
#include <algorithm>

/*!
 * \brief The bounded_int_sequence class implements the doubly-linked-list concept for unsigned integers with
 * the special constraint that each value needs to be unique in the list which is continuous in space.
 *
 * Furthermore the list is implemented with two arrays
 * and each integer-value in the sequence is represented by an index in those arrays which means that
 * large values increase memory-usage proportional to that value. A positive aspect is that the
 * allocated heap memory is continuous which may increase performance and reduce heap-fragmentation.
 * The use-cases of this class are probably very limited because it is
 * very constraining about its properties. One possible use-case is the lru-cache.
 *
 * The template parameter only allows unsigned integral types.
 */
template<typename unumber_t = size_t>
class bounded_int_sequence
{
  static_assert(std::is_integral<unumber_t>::value, "template parameter needs to be an integral type");
  static_assert(std::is_unsigned<unumber_t>::value, "template parameter needs to be unsigned");
  static constexpr unumber_t max_num = std::numeric_limits<unumber_t>::max();

public:
  /*!
   * \brief creates an instance of this class
   *
   * Per default no memory is reserved for content.
   */
  bounded_int_sequence();

  /*!
   * \brief push_back adds value to the end of the list if not contained already
   * \param value is represented by the index of an array. This means that large values increase memory
   * usage by a lot.
   *
   * \return true if value was not in the list before and otherwise false
   */
  bool push_back(const unumber_t &value);

  /*!
   * \brief pop_back erases the last element of the list
   * \return true on success false otherwise
   */
  bool pop_back();

  bool push_front(const unumber_t &value);
  bool pop_front() { return erase(begin); }

  /*!
   * \brief bring_to_front takes the provided value and erases it at the current position and then pushes it
   * to the front.
   *
   * If the value is not in the list then this function has no effect.
   *
   * \param value which should get brought to the front of the lis
   * \return true if value is in the list otherwise false
   */
  bool bring_to_front(const unumber_t &value);

  /*!
   * \brief erase value from the list if existing. Otherwise this function has no effect.
   * \param value which should be removed from list
   * \return true if value was in the list, otherwise false
   */
  bool erase(const unumber_t &value);

  /*!
   * \brief rotate shifts the list to the right or left depending on the sign of the count parameter.
   *
   * The carryover is translated to the other side of the list. This means for example
   * that one shift to the right is equivalent to the \ref #bring_to_front function used on the lists back value.
   *
   * \param count defines how many times the list is shifted by one. A negative sign results in a left shift
   * and a positive sign in a right shift.
   */
  void rotate(size_t count);

  /*!
   * \brief make_begin rotates (\ref #rotate) exactly the right amount so that new_begin value is the new
   * front of the list.
   * \param new_begin defines the new front of the list. If new_begin is not in the list this function has no effect.
   * \return true if new_begin is in the list
   */
  bool make_begin(const unumber_t &new_begin);

  /*!
   * \brief insert inserts the value before next.
   * \param next needs to be in the list for this function to have an effect.
   * \param value to be inserted
   * \return true if next is in the list and false otherwise.
   */
  bool insert(const unumber_t &next, const unumber_t &value);

  /*!
   * \brief clear removes all elements from the list
   * \param reduce_capacity defines if the capacity of the underlying data-structure
   * gets reduced or remains the same. The default is true which means that the capacity gets reduced.
   */
  void clear(bool reduce_capacity = true);

  /*!
   * \brief reserve reserves memory for new_size amount of elements.
   * \param new_size defines the new capacity. If new_size is smaller than the number of elements currently
   * in the list than this function has no effect.
   */
  void reserve(size_t new_size);

  /*!
   * \brief back has undefined behavious if list is empty
   * \return back of the list
   */
  const unumber_t &back() const;
  bool empty() const;
  bool contains(const unumber_t &index) const;
  /*!
   * \brief get_begin has undefined behaviour if list is empty
   * \return value at the front of the list
   */
  unumber_t get_begin() const;

#ifdef TEST
  std::vector<unumber_t> to_vector() const;
#endif

private:
  bool enlarge(size_t new_size);

private:
  std::vector<unumber_t> forwards;
  std::vector<unumber_t> backwards;
  unumber_t begin;
};

// ============== CLASS IMPLEMENTATION ================


template<typename unumber_t>
bounded_int_sequence<unumber_t>::bounded_int_sequence()
{
  clear();
}

template<typename unumber_t>
bool bounded_int_sequence<unumber_t>::push_back(const unumber_t &value)
{
  if (!contains(value)) {
    bool was_empty = empty();
    if (enlarge(static_cast<size_t>(value) + 1)) {
      // slot free and at the end of forwards and backwars vector
      // list can also be empty at this point
      if (!was_empty) {
        unumber_t last = backwards[begin];
        backwards[begin] = value;
        backwards[value] = last;
        forwards[value] = begin;
        forwards[last] = value;
      } else {
        begin = value;
      }
    } else {
      // index is free and somewhere in the middle of forwards and backwards vector
      // list cannot be empty because otherwise forwards.size() would equal zero
      unumber_t last = backwards[begin];
      backwards[begin] = value;
      backwards[value] = last;
      forwards[value] = begin;
      forwards[last] = value;
    }
    return true;
  }

  return false;
}

template<typename unumber_t>
bool bounded_int_sequence<unumber_t>::pop_back()
{
  if (!empty())
    return erase(back());
}

template<typename unumber_t>
bool bounded_int_sequence<unumber_t>::push_front(const unumber_t &value)
{
  if (!empty())
    return insert(begin, value);
  else {
    return push_back(value);
  }
}

template<typename unumber_t>
bool bounded_int_sequence<unumber_t>::bring_to_front(const unumber_t &value)
{
  if (erase(value)) {
    insert(begin, value); // must be successful
    return true;
  }
  return false;
}

template<typename unumber_t>
bool bounded_int_sequence<unumber_t>::erase(const unumber_t &value)
{
  if (contains(value)) {
    if (forwards[value] == value) { // sequence contains only this value
      clear(false); // the false prevents capacity loss
    } else {
      unumber_t next = forwards[value];
      unumber_t prev = backwards[value];

      forwards[value] = value;
      backwards[value] = value;

      forwards[prev] = next;
      backwards[next] = prev;

      if (begin == value)
        begin = next;
    }

    return true;
  }
  return false;
}

template<typename unumber_t>
void bounded_int_sequence<unumber_t>::rotate(size_t count)
{
  if (empty())
    return;

  if (count >= 0) {
    for (size_t i = 0; i < count; ++i) {
      begin = forwards[begin];
    }
  } else {
    for (size_t i = 0; i > count; --i) {
      begin = backwards[begin];
    }
  }
}

template<typename unumber_t>
bool bounded_int_sequence<unumber_t>::make_begin(const unumber_t& new_begin)
{
  if (contains(new_begin)) {
    begin = new_begin;
    return true;
  }
  return false;
}

template<typename unumber_t>
bool bounded_int_sequence<unumber_t>::insert(const unumber_t& next, const unumber_t& value)
{
  // Principle: Make next to the new begin, push back value and then rotate
  // again to restore old begin or value as the new begin of the sequence

  if (contains(next)) {
    unumber_t begin_cpy = begin;
    make_begin(next);
    bool res = push_back(value);

    if (res && next == begin_cpy) {
      make_begin(value);
    } else {
      make_begin(begin_cpy);
    }

    return res;
  }
  return false;
}

template<typename unumber_t>
const unumber_t& bounded_int_sequence<unumber_t>::back() const { return backwards[begin]; }

template<typename unumber_t>
bool bounded_int_sequence<unumber_t>::empty() const { return forwards.empty(); }

template<typename unumber_t>
bool bounded_int_sequence<unumber_t>::contains(const unumber_t& index) const
{
  return static_cast<size_t>(index) < forwards.size()
      && (forwards[index] != index || index == begin);
}

template<typename unumber_t>
void bounded_int_sequence<unumber_t>::clear(bool reduce_capacity)
{
  begin = max_num;

  if (reduce_capacity) {
    forwards.clear();
    backwards.clear();
  } else {
    forwards.resize(0);
    backwards.resize(0);
  }
}

template<typename unumber_t>
void bounded_int_sequence<unumber_t>::reserve(size_t new_size)
{
  forwards.reserve(new_size);
  backwards.reserve(new_size);
}

template<typename unumber_t>
unumber_t bounded_int_sequence<unumber_t>::get_begin() const { return begin; }

#ifdef TEST
template<typename unumber_t>
std::vector<unumber_t> bounded_int_sequence<unumber_t>::to_vector() const // used for debugging
{
  std::vector<unumber_t> vec;
  if (empty())
    return vec;

  vec.push_back(begin);
  for (unumber_t i = forwards[begin]; i != begin; i = forwards[i]) {
    vec.push_back(i);
  }
  return vec;
}
#endif

template<typename unumber_t>
bool bounded_int_sequence<unumber_t>::enlarge(size_t new_size)
{
  if (new_size > forwards.size()) {
    size_t old_size = forwards.size();
    forwards.resize(new_size);
    backwards.resize(new_size);
    for (size_t i = old_size; i < new_size; ++i) {
      forwards[i] = static_cast<unumber_t>(i);
      backwards[i] = static_cast<unumber_t>(i);
    }
    return true;
  }
  return false;
}


#endif // BOUNDED_INT_SEQUENCE_H
