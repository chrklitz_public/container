/*
 * Author: Christopher von Klitzing
 * Repository: https://gitlab.com/chrklitz_public/bitfield-std-like-container
 * License: See the LICENSE file in the git-repository
 */
#ifndef DYNAMIC_BITFIELD_H
#define DYNAMIC_BITFIELD_H


#include <vector>
#include "dynamic_bitfield_strategies.hpp"

/*!
 * \return the bitsize of the template parameter-type.
 */
template <typename IntType>
constexpr inline int bitsize() { // returns the size of the type in bits
  return sizeof(IntType) * 8;
}

/*!
 * \brief The dynamic_bitfield class is a bitfield implementation with a dynamic size. A bitfield
 * is a datastructure where numbers are encoded by the setting/unsetting a bit at the correct bit-index corresponding
 * to that number. A bitfield can be seen as a set of bounded numbers of a certain integral type.
 *
 * The particular property of this class is, that it supports different integral types including signed integral types.
 * This is done with a bi-directional mapping between the stored number and its corresponding bit-index. The mapping
 * is selectable via the template parameter templated_strategy.
 *
 * \param number_t is the template parameter which selects the type of the stored numbers. This parameter needs to be
 * an integral type.
 *
 * \param templated_strategy is the template parameter which selects the strategy of the bit-index mapping.
 * This template parameter has the default value default_strategy. The strategy type needs to take exactly one
 * template argument specifying the integral type defined by number_t.
 */
template <typename number_t,
          template<typename> class templated_strategy = default_strategy>
class dynamic_bitfield
{
  // =============== TYPES ETC ===================
private:
  static_assert (std::is_integral<number_t>::value, "Template parameter number_t must be of an integral type.");

  using unumber_t = typename std::make_unsigned<number_t>::type;
  using container_t = std::vector<number_t>;
  using atom_t = typename container_t::value_type;
  using selft_type = dynamic_bitfield;
  static constexpr unumber_t atomsize = bitsize<number_t>();

public:
  using key_type = number_t;
  using value_type = number_t;
  using strategy = templated_strategy<number_t>;

  // ======== HELPER FUNCTIONS (PRIVATE) =========
private:
  inline unumber_t atom_count() const { // shortcut for casting data.size() into unumber type
    return static_cast<unumber_t>(data.size());
  }
  // checks if the bit at that bit-index is set or not
  inline bool contains_index(const unumber_t &bit_index) const {
    if (bit_index / atomsize >= atom_count())
      return false;

    return (data[bit_index / atomsize] & (1 << (bit_index % atomsize))) != 0;
  }

  // ================ ITERATOR ===================
private:
  friend class const_iterator_native<selft_type>;

public:
  using const_iterator = const_iterator_native<selft_type>;

  const_iterator begin() const {
    return const_iterator(this, 0);
  }
  const_iterator end() const {
    return const_iterator(this);
  }

  const_iterator cbegin() const {
    return begin();
  }
  const_iterator cend() const {
    return end();
  }


  // ======== OTHER FUNCTIONS (PUBLIC) ==========
public:

  /*!
   * \brief insert adds a number to the bitset by setting the corresponding bit.
   * \param num is the number which should be inserted.
   * \return true if the number wasn't already in the bitfield.
   */
  bool insert(const number_t &num) {
    unumber_t bit_index = strategy::number_to_index(num);
    unumber_t atom_index = bit_index / atomsize;

    if (atom_index >= atom_count())
      data.resize(atom_index + 1, 0);

    atom_t atom = data[atom_index];
    data[atom_index] = atom | (1 << (bit_index % atomsize));
    return atom != data[atom_index];
  }

  /*!
   * \brief erase removes the number from the bitfield by unsetting the corresponding bit.
   * \param num is the number which should be removed.
   */
  void erase(const number_t &num) {
    unumber_t bit_index = strategy::number_to_index(num);
    if (bit_index / atomsize >= atom_count())
      return;

    atom_t atom = data[bit_index / atomsize];
    data[bit_index / atomsize] = atom & ~(1 << (bit_index % atomsize));
  }

  /*!
   * \brief contains checks if a number is in the bitfield by checking if its corresponding bit is set.
   * \param num is the number which should be checked.
   * \return true if the number is in the bitfield, else otherwise.
   */
  bool contains(const number_t &num) const {
    unumber_t bit_index = strategy::number_to_index(num);
    return contains_index(bit_index);
  }

#ifdef TEST
  std::vector<number_t> toVector() const { // just for debugging and testing
    std::vector<number_t> vec;
    if (contains_index(0))
      vec.push_back(strategy::index_to_number(0));
    for (unumber_t i = 1; i != static_cast<unumber_t>(0); ++i) {
      if (contains_index(i))
        vec.push_back(strategy::index_to_number(i));
    }
    return vec;
  }
#endif

  // ========== ATTRIBUTES (PRIVATE) ============
private:
  container_t data;
};

// ============ Iterator-Definition ============





#endif // DYNAMIC_BITFIELD_H
