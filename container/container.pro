TEMPLATE = app
CONFIG += c++11
CONFIG -= qt
CONFIG += console
CONFIG -= debug_and_release debug_and_release_target

DEFINES += CONTAINER_LIB
DEFINES += QT_DEPRECATED_WARNINGS

include(boost_include.pri)


SOURCES +=

HEADERS += \
  container/bounded_int_sequence.hpp \
  container/dynamic_bitfield.hpp \
  container/dynamic_bitfield_iterator.hpp \
  container/dynamic_bitfield_strategies.hpp \
  container/lru_cache.hpp

test {
  DEFINES += TEST
  include(test/testsources.pri)
} else {
  SOURCES += container/main.cpp
}
