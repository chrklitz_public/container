INCLUDEPATH += $$(BOOST_ROOT)
LIBS *= -L$$(BOOST_ROOT)/stage/lib
win32 {
  SHARED_LIB_FILES = $$files($$(BOOST_ROOT)/stage/lib/*.dll)
  for(FILE, SHARED_LIB_FILES) {
    BASENAME = $$basename(FILE)
    LIBS += -l$$replace(BASENAME,.dll,)
  }
}
unix {
  LIBS += -lboost_unit_test_framework
}
